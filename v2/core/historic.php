
<?php
require_once '../vendor/autoload.php';
require_once 'mirage.php';
$cookie = $_POST["cookie"];


$json = json_decode(file_get_contents("../tmp/".$cookie."_stutt-info.json"), true);

switch ($_POST["option"]) {
    case "fetch":
        echo json_encode($json["historic"]);
        break;
    case "display":
        $ts = $_POST["timestamp"];
        echo $json["historic"][$ts]["b64"];
        break;
    case "update":
        $ts = $_POST["timestamp"];
        $array = $json["historic"];
        $n = array_keys($array); //<---- Grab all the keys of your actual array and put in another array
        $count = array_search($ts, $n); //<--- Returns the position of the offset from this array using search
        $new_arr = array_slice($array, 0, $count + 1, true);//<--- Slice it with the 0 index as start and position+1 as the length parameter.
        $json["historic"] = $new_arr;
        file_put_contents("../tmp/".$cookie."_stutt-info.json", json_encode($json));
        break;
}

?>
