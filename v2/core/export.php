
<?php
require_once '../vendor/autoload.php';
require_once 'mirage.php';
ini_set('max_execution_time', 30000);
ini_set('memory_limit', '400M');



switch ($_POST["dpi"]) {
    case "96" :
        $r = 1;
        break;
    case "150" :
        $r = 5.9;
        break;
    case "300" :
        $r = 11.8;
        break;

}



$cookie = $_POST["cookie"];
$ts_array = explode(",",$_POST["logs"]);
$json = json_decode(file_get_contents("../tmp/".$cookie."_stutt-info.json"), true);

$tmpFile = "../tmp/img_tmp.jpg"; // todo : get the file from the initial form ?

$h = intval($json["height"])*$r;
$w = intval($json["width"])*$r;
$text = $json["text"];
$im_posx = intval($json["im_posx"])*$r;
$im_posy = intval($json["im_posy"])*$r;
$im_w = intval($json["im_w"])*$r;
$im_h = intval($json["im_h"])*$r;
$fs = intval($json["font-size"])*$r;
$id = $json["id"];
// ********************* //


// generate the base png //

$photo_to_paste = $tmpFile;

$img = imagecreatetruecolor($w, $h);
imagealphablending($img, false);
$transparency = imagecolorallocatealpha($img, 0, 0, 0, 127);
imagefill($img, 0, 0, $transparency);
imagesavealpha($img, true);

$white = imagecolorallocate($img, 255, 255, 255);
$black = imagecolorallocate($img, 0, 0, 0);

$font_path = '../assets/font/Syne-Extra.ttf';
$fontsize = $fs;
// 0.75 font ratio px to pt
$textbr = \andrewgjohnson\linebreaks4imagettftext($fontsize*0.75, 0, $font_path, $text, $w);
imagettftext($img, $fontsize*0.75,0, 0, $fontsize*0.75, $white, $font_path, $textbr);
// imagescale
list($width, $height) = getimagesize($photo_to_paste);
$src = imagecreatefromjpeg($photo_to_paste);
$im2 = imagecreatetruecolor($im_w, $im_h);
imagecopyresampled($im2, $src, 0, 0, 0, 0, $im_w, $im_h, $width, $height);

imagefilter($im2, IMG_FILTER_GRAYSCALE);
imagefilter($im2, IMG_FILTER_CONTRAST, -100);


// copy image onto the canvas
imagecopy($img, $im2, $im_posx, $im_posy,0, 0,$im_w, $im_h);

$file = '../tmp/'.$cookie.'_HD.png';

imagepng($img, $file);
imagedestroy($img);

$output = base64_encode(file_get_contents($file));

$i = 0;
$len = count($ts_array);

$channel = explode(";",$json["historic"][$ts_array[$len-1]]["data"]);
//print_r($channel);
//
$repeat = intval($channel[1]);
$bandwidth = intval($channel[2]);
$offset = intval($channel[3]);
$gutter = intval($channel[4])*$r;
$color = $channel[0];

$result = mmirage( $id, "data:image/png;base64,".$output, $repeat, $bandwidth, $offset, $gutter, $color, true );



echo $result;
// file_put_contents('../export/'.$cookie.'-stutt_HD.png', base64_decode($result));
// return base64_decode($result);
unlink($file);




?>
