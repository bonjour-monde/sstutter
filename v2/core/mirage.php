
<?php
require_once '../vendor/autoload.php';

function mmirage($id, $pic, $repeat, $bandwidth, $offset, $gutter, $color, $export_end){
    $tmp = explode(",", $pic);
    $data = base64_decode($tmp[1]);

    //transparent background
    $formImage = imagecreatefromstring($data);
    imagealphablending($formImage, false);
    $transparency_01 = imagecolorallocatealpha($formImage, 0, 0, 0, 127);
    imagefill($formImage, 0, 0, $transparency_01);
    imagesavealpha($formImage, true);

    $output = imagecreatetruecolor(imagesx($formImage), imagesy($formImage));
    imagealphablending($output, false);
    $transparency_02 = imagecolorallocatealpha($output, 0, 0, 0, 127);
    imagefill($output, 0, 0, $transparency_02);
    imagesavealpha($output, true);

    $w = round(($bandwidth*imagesy($formImage)))/100;
    $o = round(($offset*imagesy($formImage)))/100;
    $r = round(($repeat*(imagesy($formImage)/($bandwidth+$gutter))))/100;

    for ($i=0; $i < $r ; $i++) {
      imagecopy($output, $formImage, ($w*$i)+($gutter*$i), 0, $o, 0, $w, imagesy($formImage));
    }
    if( $export_end == true ){

        //color the image
        $hex = $color;
        list($red, $green, $blue) = sscanf($hex, "#%02x%02x%02x");
        //
        // imagefilter($output, IMG_FILTER_COLORIZE, $red ,$green, $blue);
        //
        // $width  = imagesx($output);
        // $height = imagesy($output);
        // $backgroundImg = imagecreatetruecolor($width, $height);
        // $color = imagecolorSllocate($backgroundImg, $red, $green, $blue);
        // imagefill($backgroundImg, 0, 0, $color);
        //
        // //copy original image to background
        // imagecopy($backgroundImg, $output, 0, 0, 0, 0, $width, $height);
        //
        // $file = '../tmp/'.$id.'.png';
        // imagepng($backgroundImg, $file);
        // imagedestroy($backgroundImg);
        //
        // $b = base64_encode(file_get_contents($file));
        // unlink($file);


        /// $output

        /* Create an Imagick with the right color */
        $width  = imagesx($output);
        $height = imagesy($output);
        $fond = new Imagick();
        $fond->newImage($width, $height, new ImagickPixel($hex));
        //
        /* Create the Imagick with the visual in BW*/
        $file = '../tmp/'.$id.'-exp.png';
        imagepng($output, $file);
        imagedestroy($output);
        //
        $img = new Imagick($file);
        //
        // $fond->setImageVirtualPixelMethod(Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);
        $fond->setImageArtifact('compose:args', "screen");
        $fond->compositeImage($img, Imagick::COMPOSITE_SCREEN, 0, 0);
        //
        $fond->setImageFormat("png");
        //
        //
        unlink($file);
        $b = base64_encode($fond);
        return $b;

    }else{
        //creat the image
        $file = '../tmp/'.$id.'.png';
        imagepng($output, $file);
        imagedestroy($output);

        $b = base64_encode(file_get_contents($file));
        unlink($file);
        return $b;
    }



}



?>
