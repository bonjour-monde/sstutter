# stttuter ¬ Mmirage (php - gd)

![](../_img/peek_02.gif)

### Prerequisites

This tool run on anyserver with php gd installed.

## Using the program

Copy paste the folder into your server, or start a php server at the root of the project.     
In your favorite browser you can now use the interface, upload your file, write your texte and start *stuttering* your image.     

![](../_img/peek_01.gif)

You can choose to export your image in 96dpi, 150dpi or 300 dpi. Note that for an export on 300dpi a lot of memory ressources are required.


## TO DO

* color treatment of the image (incorect at export on certain tone)
* Rythm of stuttering
* Stutter up down
