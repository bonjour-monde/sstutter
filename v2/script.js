//ratio pour de px à mm en 300dpi
var r = 11.8;
var active = false;

$(document).ready(function(){
    generateCanvas();
    $(".size").on("input", function(){
        generateCanvas();
    });

  ////** interface **////
  var draggable = $('#canvas .image'); //element
  draggable.on('mousedown', function(e){
      if(active != true){
          var dr = $(this).addClass("drag").css("cursor","move");
          $("#canvas textarea").css("pointer-event","none");
          height = dr.outerHeight();
          width = dr.outerWidth();
          ypos = dr.offset().top + height - e.pageY,
          xpos = dr.offset().left + width - e.pageX;

          $(document.body).on('mousemove', function(e){
              var itop = e.pageY + ypos - height;
              var ileft = e.pageX + xpos - width;
              if(dr.hasClass("drag")){
                  dr.offset({top: itop, left: ileft});
              }
          }).on('mouseup', function(e){
                $("#canvas textarea").css("pointer-event","auto");
                dr.removeClass("drag");
          });
      }
  });

    $("#import").click(function(){
        var formData = new FormData($('#doc')[0]);
        $.ajax({
          method: "POST",
          url: "core/import_img.php",
          data: formData,
           processData: false,
           contentType: false,
          success: function(response){
              var info = jQuery.parseJSON(response);
              console.log("url('"+info.url+"')");
              $(".image").css({
                  "background-image" : "url('"+info.url+"')",
                  "width" : info.width/r,
                  "height" : info.height/r
              })
          }
        });
    });


makeResizableDiv('.resizable')


});
function generateCanvas(){
    // var formData = new FormData($('#doc')[0]);
    var w = $('#width').val();
    var h = $('#height').val();
    // console.log(w, h)
    if(w == undefined ){
        $("#canvas").css({
            "width" : "800px",
            "height" : "600px"
        });
    }else{
        $("#canvas").css({
            "width" : w+"px",
            "height" : h+"px"
        });
    }
}

/*Make resizable div by Hung Nguyen*/
function makeResizableDiv(div) {
  const element = document.querySelector(div);
  const resizers = document.querySelectorAll(div + ' .resizer')
  const minimum_size = 20;
  let ratio = 0;
  let original_width = 0;
  let original_height = 0;
  let original_x = 0;
  let original_y = 0;
  let original_mouse_x = 0;
  let original_mouse_y = 0;
  for (let i = 0;i < resizers.length; i++) {
    const currentResizer = resizers[i];
    currentResizer.addEventListener('mousedown', function(e) {
      active = true;
      e.preventDefault()
      original_width = parseFloat(getComputedStyle(element, null).getPropertyValue('width').replace('px', ''));
      original_height = parseFloat(getComputedStyle(element, null).getPropertyValue('height').replace('px', ''));
      ratio = original_width / original_height;
      original_x = element.getBoundingClientRect().left;
      original_y = element.getBoundingClientRect().top;
      original_mouse_x = e.pageX;
      original_mouse_y = e.pageY;
      window.addEventListener('mousemove', resize)
      window.addEventListener('mouseup', stopResize)
    })

    function resize(e) {
      if (currentResizer.classList.contains('bottom-right')) {
        const width = original_width + (e.pageX - original_mouse_x);
        const height = width/ratio;
        // const height = original_height + (e.pageY - original_mouse_y);
        if (width > minimum_size) {
          element.style.width = width + 'px'
        }
        if (height > minimum_size) {
          element.style.height = height + 'px'
        }
      }
    }

    function stopResize() {
      window.removeEventListener('mousemove', resize);
      active = false;
    }
  }
}

// genere l'image de base
 // $("#load").click(function(){
 //    console.log(new FormData($('#elem')[0]));
 //   $.ajax({
 //     method: "POST",
 //     url: "core/previewer.php",
 //     data: new FormData($('#elem')[0]),
 //     processData: false,
 //     contentType: false,
 //     context: document.body,
 //     success: function(response){
 //       $('#content').html(response);
 //
 //     }
 //   });
 // });
 //
 //
 // $(".pos").click(function(){
 //   $.ajax({
 //     method: "POST",
 //     url: "core/previewer.php",
 //     data: new FormData($('#elem')[0]),
 //     processData: false,
 //     contentType: false,
 //     context: document.body,
 //     success: function(response){
 //       $('#content').html(response);
 //     }
 //   });
 // });
 //
 // $(".st").click(function(){
 //   // var elem = new Object();
 //   elem.title = $("#title").val();
 //   var base64 = $("#pre_img").attr("src");
 //
 //   var formData = new FormData($('#elem')[0]);
 //   formData.append("picture", base64);
 //
 //   $.ajax({
 //     method: "POST",
 //     url: "core/mirage.php",
 //     data: formData,
 //     processData: false,
 //     contentType: false,
 //     context: document.body,
 //     success: function(response){
 //       // console.log(response);
 //       $('#preview').html(response);
 //     }
 //   });
 // });
 //
 // $("#import").click(function(){
 //   // var elem = new Object();
 //   elem.title = $("#title").val();
 //   var base64 = $("#pre_img").attr("src");
 //   var formData = new FormData($('#elem')[0]);
 //
 //   formData.append("picture", base64);
 //   formData.append("export", "on");
 //   $.ajax({
 //     method: "POST",
 //     url: "core/export_new.php",
 //     data: formData,
 //     processData: false,
 //     contentType: false,
 //     context: document.body,
 //     success: function(response){
 //
 //       window.location.href = 'data:application/octet-stream;base64,' + response;
 //     }
 //   });
 // });
