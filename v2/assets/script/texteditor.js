const lorem = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

var control = '<div class="controls" contenteditable="false"><div class="control bold" contenteditable="false">B</div><div class="control italic" contenteditable="false">I</div><div class="control delete" contenteditable="false">X</div></div>';


//TODO :
// erase italic or bold on reselection



$(document).ready(function(){
    fontsize();
    //init follower
    $(window).on("load", overflowavoider );

    $(window).on( "resize", fontsize );
    $(window).on("keyup", overflowavoider );
    $('body').on("click", ".add", function(){
        $('.colselector').stop().toggle();
    });
    $('.page').on("click", ".text", function(){
        $(".text").removeClass("active");
        $(this).addClass("active");
    });

    $('.colselector div').click(function(){
        var c = $(this).attr("class");
        var n = c.split("_")[1];
        switch (n) {
          case '1':
            var i = '<div class="text col-1" contenteditable="true">'+lorem+'</div>';
            break;
          case '2':
            var i = '<div class="text col-2" contenteditable="true">'+lorem+'</div>';
            break;
          case '3':
            var i = '<div class="text col-3" contenteditable="true">'+lorem+'</div>';
            break;
          default:
            console.log('Sorry not sorry');
        }
        // $('.page').append(i);
        // console.log($('.text').last());
        $(i).insertAfter($('.text').last());
        $('.colselector').stop().toggle();
        overflowavoider();
    });
    $('body').on("mousedown", ".control", function(){
        var c = $(this).attr("class").split(" ")[1];
        switch (c) {
          case 'delete':
            $(".active").remove();
            break;
        case 'italic':
            var selection = getSelectedText();
            var selection_text = selection.toString();
            var detect = selection.getRangeAt(0).commonAncestorContainer.firstElementChild;
            if( detect != undefined){
                if($(detect).prop("tagName") == "I" && $(detect).is(':empty') != true){
                    console.log("lets remove it");
                    var range = selection.getRangeAt(0);
                    range.deleteContents();
                    range.insertNode(document.createTextNode(selection_text));
                }
            }else {
                var i = document.createElement('i');
                i.textContent = selection_text;

                var range = selection.getRangeAt(0);
                range.deleteContents();
                range.insertNode(i);
            }
            break;

        case 'bold':
            var selection = getSelectedText();
            var selection_text = selection.toString();
            var detect = selection.getRangeAt(0).commonAncestorContainer.firstElementChild;
            if( detect != undefined){
                if($(detect).prop("tagName") == "B" && $(detect).is(':empty') != true){
                    console.log("lets remove it");
                    var range = selection.getRangeAt(0);
                    range.deleteContents();
                    range.insertNode(document.createTextNode(selection_text));
                }
            }else{
                var i = document.createElement('b');
                i.textContent = selection_text;

                var range = selection.getRangeAt(0);
                range.deleteContents();
                range.insertNode(i);
            }

            break;

          default:
            console.log('Sorry not sorry');
        }
        //remove empty tags
        var str = $(".active").html();
        console.log(str);
        var f = str.replace(/<[^\/>][^>]*><\/[^>]+>/gim, "");
        $(".active").html(f);
        overflowavoider();
    });


});

function fontsize(){
    var ws = $(window).width();
    var fs = 148 / ws;
    $(".page").css("font-size",fs+"em");
}

function overflowavoider(){

    var b = $('.text');
    var height = 0;

    b.each(function(){
        var marg = parseInt($(this).css("margin"));
        height += $(this).height()+marg;
    });
    var h = $('.page').height();
    var limit = h-height;
    var inv = $('.textinv');
    var max = 0;
    inv.each(function(){
        var o = parseInt($(this).css("margin"));
        max += $(this).height()+o;
    });
    max = max + 30;

    if(limit <= max){
        $(".add").addClass("offset");
        $(".follower").css('top',$(".page").height()-max);
        $('.text').last().css("background","lightpink");
        //$(".overflowindicator").css("opacity","1");
        $(".page").css({
            "overflow-y" : 'scroll'
        });

    }else{
        $(".add").removeClass("offset");
        $(".follower").css('top',height);
        $('.text').last().css("background","lightgrey");
        //$(".overflowindicator").css("opacity","0");
        $(".page").css({
            "overflow" : 'hidden'
        });

    }

}

function getSelectedText(){
    t = (document.all) ? document.selection.createRange().text : document.getSelection();
    return t;
}
