//ratio pour de px à mm en 300dpi
var r = 11.8;
var active = false;

$(document).ready(function(){

    generateCanvas();

    $(".size").on("input", function(){
        generateCanvas();
    });

  ////** interface **////
  var draggable = $('#canvas .image'); //element
  draggable.on('mousedown', function(e){

      if(active != true){
          var dr = $(this).addClass("drag").css("cursor","move");
          $("#canvas textarea").css("pointer-event","none");
          height = dr.outerHeight();
          width = dr.outerWidth();
          ypos = dr.offset().top + height - e.pageY,
          xpos = dr.offset().left + width - e.pageX;

          $(document.body).on('mousemove', function(e){
              var itop = e.pageY + ypos - height;
              var ileft = e.pageX + xpos - width;
              if(dr.hasClass("drag")){
                  dr.offset({top: itop, left: ileft});
              }
          }).on('mouseup', function(e){
                $("#canvas textarea").css("pointer-event","auto");
                dr.removeClass("drag");
          });
      }
  });

    $("#import").click(function(){
        var formData = new FormData($('#doc')[0]);
        $.ajax({
          method: "POST",
          url: "core/import_img.php",
          data: formData,
           processData: false,
           contentType: false,
          success: function(response){
              var info = jQuery.parseJSON(response);
              // console.log("url('"+info.url+"')");
              $(".image").css({
                  "background-image" : "url('"+info.url+"')",
                  "width" : info.width/r,
                  "height" : info.height/r
              })
          }
        });
    });


makeResizableDiv('.resizable');

// font-size
    var textarea = $('textarea');
    textarea.bind('change input', function() {
        var input = textarea.val();
        input = input.replace(/\n\r/, '<br />');
        $("#dummy span").html(input);
        $('#dummy').textfill({
            maxFontPixels : 200
        });
        var fs = $("#dummy span").css("font-size");
        textarea.css('font-size',fs);
    });


$("#init").click(function(){
    var form = new FormData($('#doc')[0]);
    var text = $("textarea").val();
    var fs = $("textarea").css("font-size");
    var im_posx = $(".image").css("left");
    var im_posy = $(".image").css("top");
    var im_w = $(".image").css("width");
    var im_h = $(".image").css("height");

    form.append("text", text);
    form.append("fs", fs);
    form.append("im_posx", im_posx);
    form.append("im_posy", im_posy);
    form.append("im_w", im_w);
    form.append("im_h", im_h);
      $.ajax({
        method: "POST",
        url: "core/init.php",
        data: form,
        processData: false,
        contentType: false,
        context: document.body,
        success: function(response){

          $("#preview").empty();
          $("#preview").append(response);

          $("#stut_pre").css("background",$("#colorPick").spectrum("get").toHexString());

          $("#zone").show();
        }
      });
  });


//******* mmirage interface ********//
$("#colorPick").spectrum({
    color: "#000",
    showInput: true,
    preferredFormat: "hex",
    change: function(color) {
        $("#stut_pre").css("background",color.toHexString());
    }
});

$("ul").on("click", ".log", function(){
    var ts = $(this).data("timestamp");
    $('ul li').removeClass('active');
    $(this).addClass('active');
    var c = $(this).html().split(";");
    $("input[name=repeat]").val(c[1]);
    $("input[name=bandwidth]").val(c[2]);
    $("input[name=offset]").val(c[3]);
    $("input[name=gutter]").val(c[4]);
    hist("display",ts);
});

$(".st").mouseup(function(event){
    var form_stut = new FormData($('#stut_form')[0]);
    // console.log($("input[name=repeat]").val())
    var base64 = $("#pre_img").attr("src");
    form_stut.append("color", $("#colorPick").spectrum("get").toHexString());
    form_stut.append("picture", base64);
    form_stut.append("timestamp",event.timeStamp);

      $.ajax({
        method: "POST",
        url: "core/generate.php",
        data: form_stut,
        processData: false,
        contentType: false,
        context: document.body,
        success: function(response){
            if($(".active").data("timestamp")){
                var ts = $(".active").data("timestamp");
                hist("update", ts);
            }
            hist("fetch");
            $("#preview").empty();
            $("#preview").append(response);
            $("#stut_pre").css("background",$("#colorPick").spectrum("get").toHexString());

        }
      });
  });

    /* png export */
    /**************/
    $("#export").click(function(event){
        var array_log = [];
        $(".log").each(function(){
            if( !$(this).hasClass("active") ){
                array_log.push($(this).data('timestamp'));
            }else{
                array_log.push($(this).data('timestamp'));
                return false;
            }
        });
        var data = new FormData();
        data.append("logs", array_log);
        data.append("cookie", $("input[name=cookie]").val());
        data.append("dpi", $("select[name=dpi]").val());
        $.ajax({
          method: "POST",
          url: "core/export.php",
          data: data,
          processData: false,
          contentType: false,
          context: document.body,
          success: function(response){
              console.log("export okay!");
              alert("fichier prêt !")
              var imgTag ="<a class='download' download=\"file.png\" href=\"data:image/png;base64," + response + "\"  target=\"_blank\"> download </a>"
              $(".result_link").html(imgTag);
          }
        });

    });

});



function generateCanvas(){
    // var formData = new FormData($('#doc')[0]);
    var w = $('#width').val();
    var h = $('#height').val();
    // console.log(w, h)
    if(w == undefined ){
        $("textarea").css({
            "width" : "800px",
            "height" : "600px"
        });
        $('#dummy').css({
            "width" : "800px",
            "height" : "600px"
        });
    }else{
        $("textarea").css({
            "width" : w+"px",
            "height" : h+"px"
        });
        $('#dummy').css({
            "width" : w+"px",
            "height" : h+"px"
        });
    }
}

/*Make resizable div by Hung Nguyen*/
function makeResizableDiv(div) {
  const element = document.querySelector(div);
  const resizers = document.querySelectorAll(div + ' .resizer')
  const minimum_size = 20;
  let ratio = 0;
  let original_width = 0;
  let original_height = 0;
  let original_x = 0;
  let original_y = 0;
  let original_mouse_x = 0;
  let original_mouse_y = 0;
  for (let i = 0;i < resizers.length; i++) {
    const currentResizer = resizers[i];
    currentResizer.addEventListener('mousedown', function(e) {
      active = true;
      e.preventDefault()
      original_width = parseFloat(getComputedStyle(element, null).getPropertyValue('width').replace('px', ''));
      original_height = parseFloat(getComputedStyle(element, null).getPropertyValue('height').replace('px', ''));
      ratio = original_width / original_height;
      original_x = element.getBoundingClientRect().left;
      original_y = element.getBoundingClientRect().top;
      original_mouse_x = e.pageX;
      original_mouse_y = e.pageY;
      window.addEventListener('mousemove', resize)
      window.addEventListener('mouseup', stopResize)
    })

    function resize(e) {
      if (currentResizer.classList.contains('bottom-right')) {
        const width = original_width + (e.pageX - original_mouse_x);
        const height = width/ratio;
        // const height = original_height + (e.pageY - original_mouse_y);
        if (width > minimum_size) {
          element.style.width = width + 'px'
        }
        if (height > minimum_size) {
          element.style.height = height + 'px'
        }
      }
    }

    function stopResize() {
      window.removeEventListener('mousemove', resize);
      active = false;
    }
  }
}

/* ** historic ** */
/*  manipulation  */
/******************/

function hist(option, timestamp){
    /* options :
    - fetch
    - display
    - update
    */
    var info = new FormData();
    info.append("option", option);
    if( timestamp != undefined ){
        info.append("timestamp",timestamp);
    }
    info.append("cookie", $("input[name=cookie]").val());

    $.ajax({
      method: "POST",
      url: "core/historic.php",
      data: info,
      processData: false,
      contentType: false,
      context: document.body,
      success: function(response){
          switch(option) {
              case "fetch":
                var hist = JSON.parse(response);
                $("#historic").empty();
                for (var key in hist) {
                    if (hist.hasOwnProperty(key)) {
                        // console.log(key+" > "+hist[key]["data"]);
                        var list_entry = "<li class='log' data-timestamp='"+key+"'>"+hist[key]["data"]+"</li><br/>";
                        $("#historic").append(list_entry);
                    }
                }
                // code block
                break;
              case "display":
                // console.log(response);
                $('#stut_pre img').attr("src", response);
                var c = $('.active').html().split(";");
                var hex = c[0];
                $("#stut_pre").css("background",hex);
                $("#colorPick").spectrum("set", hex);

                break;
              case "update":
                // console.log("history update")
                break;
            }
      }
    });

}
