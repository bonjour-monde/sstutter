<?php

  require_once '../vendor/autoload.php';
?>

<!DOCTYPE html>
<html lang="fr" class="">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="Pragma" content="no-cache">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Stuttttter ¬ MMIRAGE</title>
  <script type="text/javascript" src="../assets/lib/jq.js"></script>
  <script type="text/javascript" src="../assets/script/texteditor.js"></script>
  <link rel="stylesheet" href="../assets/style/mirage.css">
</head>
<body>
<div class="container">
    <div class="page">
        <div class="title1 text col-1" contenteditable="true">
            Titre
        </div>

        <div class="footer_wrap">
            <div class="footer textinv col-2" contenteditable="true">
                <i>Synesthésie</i> ¬ MMAINTENANT<br/>
                8 passage de Jouy 93200 Saint-Denis<br/>
                07--69--49--30--03 | mm@mmaintenant.org<br/>
                www.mmaintenant.org<br/>
                <br/>
                Accès<br/>
                → Métro 13 (Basilique de Saint-Denis), RER D et H
                (Saint-Denis), Tram T1 (Marché de Saint-Denis)<br/>

                Informations pratiques<br/>
                • Accès par le rez-de-chaussée<br/>
                • Entrée libre sur rendez-vous et selon
                la programmation<br/>
                • Ouverture des portes une heure avant<br/>
                lors des événements<br/>
                • Bar associatif, petite restauration<br/>
                • Adhésion prix libre<br/>
            </div>
            <div class="footer textinv col-1" contenteditable="true">
                <i>Synesthésie</i> ¬ MMAINTENANT bénéficie du soutien du Conseil régional d’Île-de-France, du Conseil départemental de la Seine- Saint- Denis, de la Direction régionale des affaires culturelles d’Île-de-France - Ministère de la culture et de la communication et de la Ville de Saint-Denis, et du Centre commercial Basilique. <i>Synesthésie</i> ¬ MMAINTENANT est membre de Tram, réseau art contemporain Paris/Île-de-France, et de Plaine Commune Promotion.</br>
                </br>
                Type & graphic design → Bonjour Monde • Photo → Tout va bien, (prothèse), Virginie Jourdain
            </div>
        </div>

    </div>

    <div class="follower">
        <div class="add">
            +
        </div>
        <div class="colselector">
            <div class="add_1_col">
                1
            </div>
            <div class="add_2_col">
                2
            </div>
            <div class="add_3_col">
                3
            </div>
        </div>
        <div class="controls" contenteditable="false"><div class="control bold" contenteditable="false">b</div><div class="control italic" contenteditable="false">i</div><div class="control delete" contenteditable="false">X</div></div>

    </div>


</div>


</body>
</html>
