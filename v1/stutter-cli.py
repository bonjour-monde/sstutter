from PIL import Image
import os, argparse
parser = argparse.ArgumentParser()

#~~~~~~~~~~~~~~~~~~~#
#~~~~~Arguments~~~~~#
#~~~~~~~~~~~~~~~~~~~#

parser.add_argument("-c", "--crop", help="Height of the cropzone", type=int)
parser.add_argument("-o", "--offset", help="Offset from top", type=int)
parser.add_argument("-r", "--repeat", help="", type=int)
parser.add_argument('file', type=argparse.FileType('r'))

args = parser.parse_args()

#~~~~~~~~~~~~~~~~~~~#
#~~~~~~~~var~~~~~~~~#
#~~~~~~~~~~~~~~~~~~~#

height = args.crop if args.crop is not None else 1
offset = args.offset if args.offset is not None else 0
repeat = args.repeat if args.repeat is not None else 2
infile = args.file.name
outfile = os.path.splitext(infile)[0] + ".thumbnail"

#~~~~~~~~~~~~~~~~~~~#
#~~~~~~~~~~~~~#
#~~~~~~~~~~~~~~~~~~~#

if infile != outfile :
    try:
        im = Image.open(infile)
        width = im.size[0]
        # print(im.size)
        #croping img (left, upper, right, lower)
        box = (0, offset,width, height+offset)
        for i in range(repeat):
            region = im.crop(box)
            position = (0,(height+offset)*i)
            print(i)
            im.paste(region, position)
        im.save(outfile, "JPEG")
        im.show()
    except IOError:
        print("I try my best but I can't use", infile,"/n I'm doing what I can !")
